# Voting: Demo instruction

1. Clone demo project from [Gitlab](https://gitlab.com/tks-sandbox/demo/voting)

    ```sh
    git clone git@gitlab.com:tks-sandbox/demo/voting.git

    cd voting
    ```

2. Create new branch `demo` from `master`

    ```sh
    git checkout -b demo master
    ```

3. Download kubecconfig from [TKS Portal](https://ui.dev.nxcp.trueidc.com/clusters) then copy to project directory

    ```sh
    cp $(ls -t ~/Downloads/*.kubeconfig | head -n 1 ) ./

    git add .
    git commit -a -m "Add kubeconfig for deploy"
    ```

4. Deploy by create and push tag

    ```sh
    git tag -af v1.0.0 -m "Release version 1.0.0"
    git push origin --tags -f
    ```

    Then go to [Gitlab Pipeline](https://gitlab.com/tks-sandbox/demo/voting/-/pipelines) to see build and deploy progress

5. Update choice in `setting-values.yaml` then release new version

    ```sh
    git commit -a -m "Update choice in configuration"

    git tag -af v1.1.0 -m "Release version 1.1.0 with new choice"
    git push origin --tags -f
    ```

